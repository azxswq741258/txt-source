感謝各位讀者看完《加速世界18　黑衣雙劍士》。

首先，我要為本書再度讓讀者等了整整八個月這件事致歉。（注：此指日版）我會努力讓下一集恢復以前的出書步調，還請各位讀者見諒！

好了，在這第18集，「四大元素」的最後一人——Graphite　Edge終於登場，這樣第一代黑暗星雲的幹部群也終於齊眾一堂。只是黑雪公主與楓子他們對於這次重逢，似乎並不怎麼感慨……（笑）。Graph雖然在態度上、定位上，還有必殺技的名稱等等，很多地方都令人起疑，但他是寶貴（？）的男性角色，還請各位讀者多多支持！

說來離題，但先前《加速世界》原則上都是以主角春雪的第三人稱單一觀點寫的。然而單一觀點也就表示「只能寫春雪看過聽過的事情」，隨著登場人物的增加，讓多個場景同時進行的情形增加後，只靠春雪觀點就實在無法順利敘事，所以大概從第15集左右，追加了以其他人物觀點描寫的部分。目前有過黑雪公主、Pard小姐、Magenta　Scissor也就是小田切累的主觀場景，在這一集裡更追加了四野宮謠、Chocolat　Puppeteer也就是奈胡志帆子的主觀場景。

可以寫的事情增加，也就表示非寫不可的事情也會跟著增加，所以在劇情的推進上，我並不希望增加太多登場人物的觀點，但相對的用新觀點寫起來卻也相當開心。尤其志帆子她們Petit　Paquet三人組的場景，更有著我不曾寫過的日常系喜劇輕小說風格，連我自己寫起來都覺得很新鮮（笑）。我希望將來能多找些機會，用更長的篇幅試著寫寫看她們的部分。

劇情方面已經（或許該說是終於……）要朝「禁城」與「加速研究社」這兩大主軸邁向解決。過去只灑不收的許多伏筆，我也打算努力回收，還請各位讀者多多給予支持與愛護！

另外這一集也和上集一樣，收錄了當初作為電視版動畫《加速世界》BD&DVD特典而新寫的短篇作品〈紅焰的軌跡〉。在此謹對給予許可的各位相關人士，以及支持動畫版的各位讀者，鄭重表達我的謝意。

另外也要謝謝將這次新登場的真人版Petit　Paquet三個女生與鈷錳姊妹花畫得非常可愛的插畫師HIMA老師，還有多方費心進行調整與整理工作流量的責任編輯三木先生！那麼我們第19集再會！

二〇一五年四月某日　　川原礫
